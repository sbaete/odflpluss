% Steven Baete
% NYU SOM CBI
% August 2017

close all;
clear all;

%% set path
p = genpath('scripts');
addpath(p);

%% list of cases
% system(['ls */*.fib.gz > fiblist.txt']);
fid = fopen('fiblist.txt');
fib = textscan(fid,'%s\n');
fclose(fid);
fib = fib{1};

%% load jacobian determinant maps

for i = 1:length(fib)
    display([' [' num2str(i) '/' num2str(length(fib)) '] ']);
    fibt = fib_obj(fib{i});
    tmp = fibt.jdet;
    if (i == 1)
        jdetall = zeros([size(tmp),length(fib)]);
    end;
    jdetall(:,:,:,i) = tmp;
end;

%% 
figure;hold on;
imagesc(squeeze(montageSB(squeeze(jdetall(:,:,42,:)))));
title('Jdet');axis off;colorbar

%%
figure;hold on;
imagesc(squeeze(mean(squeeze(jdetall(:,:,42,:)),3)));
title('Jdet');axis off;colorbar

figure;hold on;
imagesc(squeeze(std(squeeze(jdetall(:,:,42,:)),[],3)));
title('Jdet-std');axis off;colorbar

%%
figure;hold on;
imagesc(squeeze(mean(squeeze(montageSB(jdetall(:,:,:,:))),3)));
title('Jdet');axis off;colorbar

figure;hold on;
imagesc(squeeze(std(squeeze(montageSB(jdetall(:,:,:,:))),[],3)));
title('Jdet-std');axis off;colorbar
