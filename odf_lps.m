% Steven Baete
% NYU SOM CBI
% August 2017

% -----------------------------------------------------------------------
% This method has been published in:
%  Baete, Chen, Lin, Wang, Otazo, Boada. Low Rank plus Sparse Decomposition 
%	of ODFs for Improved Detection of Group-level Differences and 
%	Variable Correlations in White Matter. 
%	Accepted for publication in NeuroImage, 2018.
% -----------------------------------------------------------------------

% Example usage of the L+S ODF decomposition using HCP data. See readme.md.

close all;
clear all;

%% input parameters

% folder in which the processing will take place
[fold,~,~]=fileparts(mfilename('fullpath'));

% location of FSL randomise
[~,b]=system('which randomise');
[fslfold,~,~]=fileparts(b);

% fibgz template for DSIStudio recon
dsistudiofold='/usr/local/pkg/dsistudio/20160823/';
fibtempl=[dsistudiofold 'HCP842_2mm.fib.gz'];

% reference nii to plot results on
% ./dsi_studio --action=exp --source=HCP842_2mm.fib.gz --export=fa0
% mrtransform -force -template HCP842_2mm.fib.gz.fa0.nii.gz mni_icbm152_t1_tal_nlin_asym_09a.nii.gz HCP842_T1_2mm.nii.gz
templatefile = 'HCP842_T1_2mm.nii.gz';

% name of nii of each individual case
niifilename = 'data_2mm.nii.gz';

%% set path
p = genpath('scripts');
addpath(p);

%% prep
% folder with the HCP data
% datafold = '/media/baetes01/baetelabspace/HCP/';

% copy diffusion data to the processing folder
% system(['./scripts/HCPscripts/cppreproc ' datafold ' ' fold ' ' niifilename]);

% generate src-files for each case
system(['./scripts/HCPscripts/dsistudio_createsrcgz ' fold ' ' niifilename ' ' dsistudiofold]);

% generate fib-files for each case
system(['./scripts/HCPscripts/dsistudio_createfibgz ' fold ' ' fibtempl ' ' dsistudiofold]);

params = {'BMI';};
%% generate the design-matrix and design-contrast files for the different tests
system(['ls */data.src.gz.*.fib.gz > fiblist.txt']);
fid = fopen(['fiblist.txt']);
fib = textscan(fid,'%s\n');
fclose(fid);
fib = fib{1};
generate_design_matrix(fib,fold,datafold,false,params);
% demog_stats(fib,fold,datafold,false,params);
%% loop over tests
for p = 1:length(params)
    tstr = ['lps_' params{p}]

    %% list of cases
    %system(['ls */data.src.gz.*.fib.gz > fiblist.txt']);
    fid = fopen(['fiblist_' params{p} '.txt']);
    fib = textscan(fid,'%s\n');
    fclose(fid);
    fib = fib{1};
    
    %% location of design-matrix and design-contrast file
    dmatfile=['data_' params{p} '.mat'];
    dconfile=['data_' params{p} '.con'];

    % strings for the different tests for display in the plots
    tests = {['high ' params{p} ' > low ' params{p}];
             ['low ' params{p} ' > high ' params{p}];
             'older > younger';'younger > older';
             'male > female';'female > male'};
     
    %% L+S decomposition
    odf_lps_calc(fib,templatefile,dmatfile,dconfile,'outfold',tstr);
    
    %% paramsearch for the L+S
    %region.xsel = [20:40];region.ysel = [44:49];region.zsel = [42];
    %odf_lps_calc(fib(1:100),templatefile,dmatfile,dconfile,'outfold',tstr,...
    %    'saveregion',true,'region',region);
    %odf_lps_paramsearch(tstr);
    
    %% FWE correction
    odf_lps_fwe(tstr,fslfold,dmatfile,dconfile,'permutations',1000);

    %% Save difference or correlation odf's in fib-files
    diffodf(tstr,dmatfile,dconfile);

    %% plot FWE-corrected results
    odf_lps_fwe_plot(tstr,templatefile,dmatfile,dconfile,'tests',tests);

    %% plot diffodf directions
    diffodf_plot(tstr,templatefile,dmatfile,dconfile,'silent',true,'tests',tests);
end;
