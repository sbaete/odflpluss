function [U,S,V] = svdecon(X)
% Input:
% X : m x n matrix
%
% Output:
% X = U*S*V'
%
% Description:
% Does equivalent to svd(X,'econ') but faster
%
% Vipin Vijayan (2014)
% Changes for speed and stability by Steven Baete, NYU SOM CBI (2017)

%X = bsxfun(@minus,X,mean(X,2));
[m,n] = size(X);

if  m <= n
    C = X*X';
    try
        [U,D] = eig(C);
    catch
        try
            display(' svdecon: caught a problem with eig');
            [U,D,W] = svd(C);
            D = D.*sign(diag(real(dot(U,W,1))));
        catch
            display(' svdecon: caught a second problem with eig');
            % First we compute the squared Frobenius norm of our matrix
            nA = sum(sum(C.^2));
            % Then we make this norm be meaningful for element wise comparison
            nA = nA / numel(C);
            % Finally, we smooth our matrix
            C( C.^2 < 1e-10*C ) = 0;
            C(isnan(C)) = 0;
            C(isinf(C)) = 0;
            [U,D,W] = svd(C);
            D = D.*sign(diag(real(dot(U,W,1))));
        end;
    end;
    clear C;
    
    [d,ix] = sort(abs(diag(D)),'descend');
    U = U(:,ix);    
    
    if nargout > 2
        V = X'*U;
        s = sqrt(d);
        V = bsxfun(@(x,c)x./c, V, s');
        S = diag(s);
    end
else
    C = X'*X; 
    C(isnan(C)) = 0; C(isinf(C)) = 0;
    try
        [V,D] = eig(C);
    catch
        try
            display(' svdecon: caught a problem with eig');
            [V,D,W] = svd(C);
            D = D.*sign(diag(real(dot(V,W,1))));
        catch
            display(' svdecon: caught a second problem with eig');
            % First we compute the squared Frobenius norm of our matrix
            nA = sum(sum(C.^2));
            % Then we make this norm be meaningful for element wise comparison
            nA = nA / numel(C);
            % Finally, we smooth our matrix
            C( C.^2 < 1e-10*C ) = 0;
            C(isnan(C)) = 0;
            C(isinf(C)) = 0;
            [V,D,W] = svd(C);
            D = D.*sign(diag(real(dot(V,W,1))));
        end;
    end;
    clear C;    
    
    [d,ix] = sort(abs(diag(D)),'descend');
    V = V(:,ix);    
    
    U = X*V; % convert evecs from X'*X to X*X'. the evals are the same.
    %s = sqrt(sum(U.^2,1))';
    s = sqrt(d);
    %U = bsxfun(@(x,c)x./c, U, s');
    U = U./repmat(s',[m,1]);
    S = diag(s);
end

U(isnan(U)) = 0; U(isinf(U)) = 0;
