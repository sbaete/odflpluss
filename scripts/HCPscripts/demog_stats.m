% Steven Baete
% generate FSL randomise style design-matrix and design-contrast file
% Aug 2017
% NYU SOM CBI/CAIIR

function demog_stats(fib,tempfold,dmogfold,rounddemog,params)

close all;
if (nargin < 3)
	rounddemog = false;
end;

%% which cases do we have?
for i = 1:length(fib)
    casenum(i) = str2num(fib{i}(1:6));
end;

demoglist = {'Race';'Release'};

%% load the master excell-sheet and figure gender, age, bm
% assign groups based on gender (so the algorithm runs smoothly, not really
% necessary)
demog = readtable([dmogfold filesep ...
    'RESTRICTED_all_baetes01_10_26_2016_10_18_2.csv'],...
    'Delimiter',',','ReadVariableNames',true,'FileType','text');
demog2 = readtable([dmogfold filesep ...
    'unrestricted_all_baetes01_10_26_2016_10_18_33.csv'],...
    'Delimiter',',','ReadVariableNames',true,'FileType','text');

subj = demog{1:end,'Subject'};
agel = demog{1:end,'Age_in_Yrs'};
subj2 = demog2{1:end,'Subject'};
genderl = demog2{1:end,'Gender'};

for p = 1%:length(params)
    clear age par gender groups;
    
    try
        parl = demog{1:end,params{p}};
        parlind = subj;
    catch
        parl = demog2{1:end,params{p}};
        parlind = subj2;
    end;
    for k = 1:length(demoglist)
        try
            parg{k} = demog{1:end,demoglist{k}};
            pargind{k} = subj;
        catch
            parg{k} = demog2{1:end,demoglist{k}};
            pargind{k} = subj2;
        end;
    end;
    for i = 1:length(casenum)
        ind = find(subj == casenum(i));
        age(i) = agel(ind);
        ind = find(subj2 == casenum(i));
        if (strcmp(genderl{ind},'F'))
            gender(i) = 1;
        else
            gender(i) = 2;
        end;
        groups(i) = gender(i);
        ind = find(parlind == casenum(i));
        par(i) = parl(ind);
        for k = 1:length(demoglist)
            ind = find(pargind{k} == casenum(i));
            parglist{k}{i} = parg{k}{ind};
        end;
    end;

    groupstr={'Female';'Male'};

    dmogsel={'SUBJ';'AGE';params{p};'GENDER'};
    dmog=table(casenum',age',par',gender','VariableNames',dmogsel);
    dmogsel2={demoglist{:}};
    dmog2=table(parglist{1}',parglist{2}','VariableNames',dmogsel2);
    
    %% load demographic data
    did = dmog{:,dmogsel};

    % select the cases we need
    [~, ind] = ismember(casenum, did(:,1));
    did = did(ind,:);
    nvol = length(ind);
    fibout = fib(ind);

    % remove cases with missing values
    ind = (sum(isnan(did),2)==0);
    did = did(ind,:);
    nvol = sum(ind);
    fibout = fibout(ind);
    
    %% statistics
    display(['  # volunteers  : ' num2str(nvol)]);
    display(['  female / male : ' num2str(sum(dmog{:,'GENDER'}==1)) ' / ' ...
                                  num2str(sum(dmog{:,'GENDER'}==2))]);
    display(['  age           : ' num2str(mean(dmog{:,'AGE'})) ' +- ' ...
                                  num2str(std(dmog{:,'AGE'})) ' y/o']);
    display(['  BMI           : ' num2str(mean(dmog{:,'BMI'})) ' +- ' ...
                                  num2str(std(dmog{:,'BMI'})) ' y/o']);
    for k = 1:length(demoglist)
        display(['  ' demoglist{k} ' : ']);
        opts = unique(dmog2{:,demoglist{k}});
        for j = 1:length(opts)
            display(['      ' num2str(sum(strcmp(dmog2{:,demoglist{k}},opts{j}))) '  '  opts{j}]);
        end;  
    end;
end;
