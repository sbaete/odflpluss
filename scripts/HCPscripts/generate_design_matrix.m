% Steven Baete
% generate FSL randomise style design-matrix and design-contrast file
% Aug 2017
% NYU SOM CBI/CAIIR

function generate_design_matrix_extended(fib,tempfold,dmogfold,rounddemog,params)

close all;
if (nargin < 3)
	rounddemog = false;
end;

%% test settings
l = 0;


for i = 1:length(params)
    l = l+1;
    tests{l}.main = params{i};
    tests{l}.nuis = {'AGE';'GENDER'};
end;

%% which cases do we have?
for i = 1:length(fib)
    casenum(i) = str2num(fib{i}(1:6));
end;

%% load the master excell-sheet and figure gender, age, bm
% assign groups based on gender (so the algorithm runs smoothly, not really
% necessary)
demog = readtable([dmogfold filesep ...
    'RESTRICTED_all_baetes01_10_26_2016_10_18_2.csv'],...
    'Delimiter',',','ReadVariableNames',true,'FileType','text');
demog2 = readtable([dmogfold filesep ...
    'unrestricted_all_baetes01_10_26_2016_10_18_33.csv'],...
    'Delimiter',',','ReadVariableNames',true,'FileType','text');

subj = demog{1:end,'Subject'};
agel = demog{1:end,'Age_in_Yrs'};
subj2 = demog{1:end,'Subject'};
genderl = demog2{1:end,'Gender'};

for p = 1:length(params)
    clear age par gender groups;
    
    try
        parl = demog{1:end,params{p}};
    catch
        parl = demog2{1:end,params{p}};
    end;
    for i = 1:length(casenum)
        ind = find(subj == casenum(i));
        age(i) = agel(ind);
        par(i) = parl(ind);
        ind = find(subj2 == casenum(i));
        if (strcmp(genderl{ind},'F'))
            gender(i) = 1;
        else
            gender(i) = 2;
        end;
        groups(i) = gender(i);
    end;

    groupstr={'Female';'Male'};

    dmogsel={'SUBJ';'AGE';params{p};'GENDER'};
    dmog=table(casenum',age',par',gender','VariableNames',dmogsel);

    %% load demographic data
    did = dmog{:,dmogsel};

    % select the cases we need
    [~, ind] = ismember(casenum, did(:,1));
    did = did(ind,:);
    nvol = length(ind);
    fibout = fib(ind);

    % remove cases with missing values
    ind = (sum(isnan(did),2)==0);
    did = did(ind,:);
    nvol = sum(ind);
    fibout = fibout(ind);
    
    %% write fiblist
    fid = fopen(['fiblist_' params{p} '.txt'],'w');
    for j = 1:length(fibout)
        fprintf(fid,[fibout{j} '\n']);
    end;
    fclose(fid);
    
    %% make supporting files for FSL randomise
    ntest = 2 + 2*length(tests{p}.nuis);

    dlmwrite([tempfold filesep  'data_' params{p} '.cases'],did(:,1),'precision',10);
    
    % data.con
    nvar = 2 + length(tests{p}.nuis);
    matr = zeros(2,nvar);
    matr(1:2,2) = [1;-1];     
    for j = 1:length(tests{p}.nuis)
        matr(2+(j-1)*2+(1:2),2+j) = [1;-1];
    end; 
    writeFSLrandomise([tempfold filesep 'data_' params{p} '.con'],matr);

    % data.mat
    matr = zeros(nvol,nvar);
    matr(:,1) = ones(nvol,1);
    tmp = did(:,find(strcmp(dmogsel,tests{p}.main)));
    matr(:,2) = tmp - mean(tmp);
    for j = 1:length(tests{p}.nuis)
        tmp = did(:,find(strcmp(dmogsel,tests{p}.nuis{j})));
        matr(:,2+j) = tmp - mean(tmp);
    end;
    if (rounddemog)
        writeFSLrandomise([tempfold filesep 'data_' params{p} '_round.mat'],round(matr),true);
    else		
        writeFSLrandomise([tempfold filesep 'data_' params{p} '.mat'],matr);
    end;
end;
