% Steven Baete
% NYU SOM CBI
% August 2017

function writeFSLrandomise(file,matr,rounddemog)

if (nargin < 3)
	rounddemog = false;
end;

fid = fopen(file,'w');
fprintf(fid,['/NumWaves ' num2str(size(matr,2)) '\n']);
if (strfind(file,'.con'))
    fprintf(fid,['/NumContrasts ' num2str(size(matr,1)) '\n']);
end;
if (strfind(file,'.mat'))
    fprintf(fid,['/NumPoints ' num2str(size(matr,1)) '\n']);
end;
fprintf(fid,['/PPheights 1 1' '\n']);
fprintf(fid,['/Matrix' '\n']);
for i = 1:size(matr,1)
    for j = 1:size(matr,2)
	if rounddemog
	        fprintf(fid,'%d ',matr(i,j));
	else
	        fprintf(fid,'%6.2f ',matr(i,j));
	end;
    end;
    fprintf(fid,'\n');
end;

fclose(fid);
