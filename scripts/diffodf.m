%% export difference or correlation ODFs in fib-file format

% Steven Baete
% NYU SOM CBI
% April 2016

% -----------------------------------------------------------------------
% This method has been published in:
%  Baete, Chen, Lin, Wang, Otazo, Boada. Low Rank plus Sparse Decomposition 
%	of ODFs for Improved Detection of Group-level Differences and 
%	Variable Correlations in White Matter. 
%	Accepted for publication in NeuroImage, 2018.
% -----------------------------------------------------------------------

%   Input:
%       lpsfold: folder with L+S-decomposition results
%       dmatfile,dconfile: location of design matrix and contrast file
%           using the fsl randomize format
%   Options (in 'keyword','value' -format)
%       thres: p-value threshold
%           default: 0.05

function diffodf(lpsfold,dmatfile,dconfile,varargin)

thres = 0.05;
if (nargin > 4)
    for i=1:2:size(varargin,2)
        if (~ischar(varargin{i}))
            error('odf_lps_fwe: Please use string-value pairs for input');
        end;
        switch varargin{i}
            case 'thres'
                thres = varargin{i+1};
            otherwise
                error(['odf_lps_fwe: I did not recognize the input-string ' varargin{i}]);
        end;
    end;
end;

%% load mat-file
load([lpsfold filesep 'odf_lps_mat.mat'],'mask','fibfilename','pc','pall','maxpc','faavg','b0avg');

%% how many statistical tests are we performing?
dcon = readFSLdesign(dconfile);
ntest = size(dcon,1);

p = ones(size(pall));

for jtest = 1:2:ntest
    %% read in the multiple comparison corrected p-values
    for k = 1:maxpc
        tempfold = [lpsfold filesep 'fwe_pc' num2str(k) filesep];
        % positive test
        niifname = [tempfold 'data_tfce_corrp_tstat' num2str(jtest) '.nii.gz'];
        if (~exist(niifname,'file'))
            niifname = [tempfold 'data_tfce_corrp_tstat' num2str(jtest) '.nii'];
            if (~exist(niifname,'file'))
                display('Did not find all necessary nii-files.');
                break;
            end;
        end;
        dnii1 = load_untouch_nii(niifname);
        
        % negative test
        niifname = [tempfold 'data_tfce_corrp_tstat' num2str(jtest+1) '.nii.gz'];
        if (~exist(niifname,'file'))
            niifname = [tempfold 'data_tfce_corrp_tstat' num2str(jtest+1) '.nii'];
            if (~exist(niifname,'file'))
                display('Did not find all necessary nii-files.');
                break;
            end;
        end;
        dnii2 = load_untouch_nii(niifname);
        
        % save all the p-values
        pt1 = 1-dnii1.img;pt2 = 1-dnii2.img;
        pt1 = flipdim(pt1,2); pt2 = flipdim(pt2,2);
        pt1 = pt1(mask(:)==1);pt2 = pt2(mask(:)==1);
        pt1(pt1 == 0) = 1e3;pt2(pt2 == 0) = 1e3;
        p(:,k) = min(pt1,pt2);
    end;

    %% calculate the multiple comparison corrected diff ODF
    mp = zeros(size(p,1),1);
    RODF = zeros(size(p,1),size(pc,2));
    for ik = 1:size(p,1)
        sel = p(ik,:) < thres;
        if (sum(sel) > 0)
            mp(ik) = min(p(ik,sel));
            RODF(ik,:) = squeeze(sum(pc(ik,:,sel),3));
        end;
    end;

    %% save the diff ODF in fib-files
    sistr = {'pos';'neg';};

    for si = [1,2]
        % load the file we are copying from
        fib = fib_obj(fibfilename{1});

        % make a copy of 'a' fib-file
        filen = [lpsfold filesep 'data_test' num2str(jtest) '-' num2str(jtest+1) '_' sistr{si} '.p' num2str(thres) '.fibt'];
        system(['rm ' filen]);
        fibcp = fib.copy(filen);clear fib;
        fibcp.removefields({'jdet';'gfa';'iso';'nqa0';'nqa1';'nqa2';'nqa3';'nqa4'; ...
            'rdi02L';'rdi04L';'rdi06L';'rdi08L';'rdi10L';'rdi12L';...
            'nrdi02L';'nrdi04L';'nrdi06L';'nrdi08L';'nrdi10L';'nrdi12L';});
        setstr = [];clearstr = [];

        % set the average fa0 and b0-file
        faavg(mask(:) == 0) = 0;
        faavg((mask(:) == 1) & (faavg(:) < 1e-4)) = 1e-4;
        b0avg(mask(:) == 0) = 0;
        setstr = strcat(setstr,',''fa0'',faavg');
        setstr = strcat(setstr,',''Mfa'',faavg');
        setstr = strcat(setstr,',''b0'',b0avg');

        pout = prepforout(mp(:),mask,[]);
        setstr = strcat(setstr,',''p'',pout');    
        logpout = prepforout(-log(mp(:)),mask,[]);
        setstr = strcat(setstr,',''logp'',logpout');

        odft = zeros([size(RODF,2),prod(size(mask))]);
        odftt = RODF; 
        if (si == 2)
            odftt = -odftt;
        end;   
        odftt(odftt <= 0) = 1e-6;
        odft(:,mask(:)) = permute(odftt,[2,1]);
        odft = reshape(odft,[size(RODF,2),size(mask)]); 
        setstr = strcat(setstr,',''odf'',odft');

        % calculate and set the indices
        MAX_FIBERS = 5;
        odf_vertices = fibcp.odf_vertices;
        odf_faces = fibcp.odf_faces;
        odfHalfLength = size(odf_vertices,2)/2;
        odf_faces_fp = odf_faces + 1;
        odf_faces_fp = odf_faces_fp - (odf_faces_fp > 2*odfHalfLength)*(2*odfHalfLength);
        maxPeaks = 0;
        for ff = 0:MAX_FIBERS-1
           fal{ff+1} = ones(size(RODF,1),1)*1e-4;
           indexl{ff+1} = zeros(size(RODF,1),1);
        end;

        for mm = 1:size(RODF,1)
            if (mp(mm) < thres && mp(mm) > 0)
                odf = odftt(mm,:);
                peaksTmp = find_peak_sb(cat(2,odf, odf),odf_faces_fp);
                if (length(peaksTmp)/2 > maxPeaks)
                   maxPeaks = length(peaksTmp)/2;
                end;
                %get the peaks and odf values
                pks = peaksTmp(1:2:end);
                pks = pks(pks <= length(odf));
                odfVals = odf(pks);
                %sort by odf value to arrange into fa0, fa1, etc.
                [odfVals, idx] = sort(odfVals,'descend');
                min_odf = min(odf);
                pks = pks(idx);
                if (isempty(pks)), pks = [1];odfVals = 1e-4; end;
                if (odfVals(1) < 1e-4+min_odf), odfVals(1) = 1e-4+min_odf; end;
                fal{1}(mm) = 1e-4;
                for ff = 0:MAX_FIBERS-1
                   if (ff+1 <= length(pks))
                      fal{ff+1}(mm) = odfVals(ff+1);
                      indexl{ff+1}(mm) = pks(ff+1) - 1;
                   else
                      break;
                   end;
                end; 
            end;
        end;
        fal{1}(:) = max(fal{1}(:),1e-4);

        for ff = 0:MAX_FIBERS-1         
           eval(sprintf('fa%d = zeros(size(mask));', ff));
           eval(sprintf('fa%d(mask == 1) = fal{%d};', ff, ff+1));
%            if ( si < 3)
%                 eval(sprintf('fa%d(faavg(:) <= 0) = 1e-4;', ff, ff+1));
%            end;
           eval(['setstr = strcat(setstr,'',''''fa' num2str(ff) ''''',fa' num2str(ff) ''');']);
           eval(['clearstr = strcat(clearstr,'' fa' num2str(ff) ''');']);
           eval(sprintf('index%d = zeros(size(mask));', ff));
           eval(sprintf('index%d(mask == 1) = indexl{%d};', ff, ff+1));
           eval(['setstr = strcat(setstr,'',''''index' num2str(ff) ''''',index' num2str(ff) ''');']);
           eval(['clearstr = strcat(clearstr,'' index' num2str(ff) ''');']);
        end;
        eval(['fibcp.setvolume(' setstr(2:end) ');']);
        eval(['clear' clearstr ]);

        % close the new fib-file
        fibcp.delete;
        clear fibcp;
        system(['rm ' filen]);
    end;
end;
