%% L+S decomposition of ODFs of groups of volunteers
%  plot directions identified from difference or correlation odfs

% Steven Baete
% NYU SOM CBI
% April 2016

% -----------------------------------------------------------------------
% This method has been published in:
%  Baete, Chen, Lin, Wang, Otazo, Boada. Low Rank plus Sparse Decomposition 
%	of ODFs for Improved Detection of Group-level Differences and 
%	Variable Correlations in White Matter. 
%	Accepted for publication in NeuroImage, 2018.
% -----------------------------------------------------------------------

function diffodf_plot(lpsfold,templatefile,dmatfile,dconfile,varargin)

pthres = 0.05;
qathres = 0.015;
qaorigthres = [];
silent = true;
tests = [];
if (nargin > 4)
    for i=1:2:size(varargin,2)
        if (~ischar(varargin{i}))
            error('diffodf_plot: Please use string-value pairs for input');
        end;
        switch varargin{i}  
            case 'pthres'
                pthres = varargin{i+1};
            case 'qathres'
                qathres = varargin{i+1};
            case 'qaorigthres'
                qaorigthres = varargin{i+1};
            case 'silent'
                silent = varargin{i+1};                
            case 'tests'
                tests = varargin{i+1};
            otherwise
                error(['diffodf_plot: I did not recognize the input-string ' varargin{i}]);
        end;
    end;
end;

if (isempty(qaorigthres)), qaorigthres = qathres; end;

display(['   	   data located in ' lpsfold]);

%% how many statistical tests are we performing?
dcon = readFSLdesign(dconfile);
ntest = size(dcon,1);

if (isempty(tests))
    tests = cell(ntest,1);
    for j = 1:ntest
        tests{j} = ['test ' num2str(j)];
    end;
end;

%% loop over the saved fib-files
for jtest = 1:2:ntest
    sistr = {'pos';'neg';};

    for si = [1,2]
        fibfile = [lpsfold filesep 'data_test' num2str(jtest) '-' ...
            num2str(jtest+1) '_' sistr{si} '.p' num2str(pthres) '.fib.gz'];
        titel = [tests{jtest+si-1} ',  p < ' num2str(pthres) ...
				', QA > ' num2str(qathres)];
            
        fileout = strrep(fibfile,'.fib.gz',...
            ['.qa' num2str(qathres) '.dir.sel.png']);        
        slisel.z = [25:2:56];
        montageFiberDirOnTemplate(templatefile,fibfile,fileout,...
            silent,titel,[],qathres,slisel,qaorigthres);
        
        
        fileout = strrep(fibfile,'.fib.gz',...
            ['.qa' num2str(qathres) '.dir.png']);        
        slisel.z = [17:58];
        montageFiberDirOnTemplate(templatefile,fibfile,fileout,...
            silent,titel,[],qathres,slisel,qaorigthres);
    end;
end;
