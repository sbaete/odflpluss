%% Print the mean rank, sparsity and oversampling of the ODF-matrices

% Steven Baete
% NYU SOM CBI
% Okt 2017

%   Input:
%       lpsfold: folder with L+S-decomposition results

function display_rank_spars_oversampl(lpsfold)

%% load the results
load([lpsfold filesep 'odf_lps_mat.mat'],...
    'rankL','cardS','oversamplingM');

display([' Mean Norm Rank of L : ' num2str(mean(rankL)) ...
        '  std ' num2str(std(rankL))]);
display([' Mean Norm Card of S : ' num2str(mean(cardS)) ...
        '  std ' num2str(std(cardS))]);
display([' Oversampling of M   : ' num2str(mean(oversamplingM)) ...
    '  std ' num2str(std(oversamplingM))]);