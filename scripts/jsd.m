% Jensen-Shannon divergence, see Cohen-Adad, JMRI 2011, 33(5): 1194-1208

% Steven Baete
% NYU SOM CBI
% Oktober 2017

function r = jsd(A,B)

% the divergences work on probability distributions which have a total sum of 1
for i = 1:size(A,2)
    for j = 1:size(A,3)
        a = A(:,i,j);
        a = a - min(a)*0.9999;
        a = a/max(a);
        nana = ~isnan(a);Nnana = sum(nana);
        A(:,i,j) = a/(sum(a(nana),1)/Nnana);
        b = B(:,i,j);
        b = b - min(b)*0.9999;
        b = b/max(b);
        nanb = ~isnan(b);Nnanb = sum(nanb);
        B(:,i,j) = b/(sum(b(nanb),1)/Nnanb);
    end;
end;

C = (A+B)/2;

A(isnan(A)) = 0;
B(isnan(B)) = 0;
C(isnan(C)) = 0;

a = kld(A,C);
b = kld(B,C);
r = (a+b)/2;
