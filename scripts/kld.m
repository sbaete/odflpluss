% Kullback-Leibler divergence, see Cohen-Adad, JMRI 2011, 33(5): 1194-1208

% Steven Baete
% NYU SOM CBI
% Oktober 2017

function r = kld(A,B)

% the divergences work on probability distributions which have a total sum of 1
s = size(A);
r = zeros(size(A,2),size(A,3));
a = zeros(s(1),1);b = zeros(s(1),1);
for i = 1:size(A,2)
    for j = 1:size(A,3)
        a(:) = A(:,i,j);
        b(:) = B(:,i,j);
        
        a = a-min(a)*0.9999;
        a = a/max(a);
        b = b-min(b)*0.9999;
        b = b/max(b);
        
        nana = ~isnan(a);Nnana = sum(nana);
        a = a/(sum(a(nana),1)/Nnana);
        nanb = ~isnan(b);Nnanb = sum(nanb);
        b = b/(sum(b(nanb),1)/Nnanb);
        
        a(~nana) = 0;
        b(~nanb) = 0;
        
        temp = a.*log10(a./b);
        temp(isnan(temp)) = 0;
        r(i,j) = sum(temp);
    end;
end;

r(isnan(r)) = 0;
