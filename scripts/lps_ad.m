% Steven Baete
% NYU SOM CBI
% September 2015

% Based on lps_tv.m from Ricardo Otazo (2015)
% Alternating Directions from Candes et al, Journal of the ACM, 
%   Vol 58, No 3, Art 11, 2011, p28

function [L,S,score,coeff,Mm,mu] = lps_ad(M,lambda,mu,centered,maxout,niter)

normL2 = @(x) sum(sum(x.^2).^(1/2));

s = size(M);
M=reshape(M,[prod(s(1:(end-1))),s(end)]);

S = zeros(size(M));
Y = zeros(size(M));

if (nargin<4)
    centered = false;
end;
if (centered)
    Mm = mean(M,2);
    M = M - repmat(Mm,[1,s(end)]);
else
    Mm = [];
end;
if (nargin<5)
    maxout = 5;
end;
maxout = min(maxout,size(M,1));

if (nargin<2 || isempty(lambda))
    lambda = 1/sqrt(min(size(M)));
end;
if (nargin<3 || isempty(mu))
    mu = 25*prod(size(M))/(norm(M,1)); 
end;
if (mu == -1)
    mu = prod(size(M))/(4*norm(M,1));
end;

%   lambda
if (nargin<6 || isempty(niter))
   niter = 50;
end;
l = 0;

normM = normL2(M);

while (1)
    [Ut,St,Vt]=svdecon(M-S+1/mu*Y);
    St=diag(SoftThresh(diag(St),St(1)*1/mu));
    L=Ut*St*Vt';
    
    S = SoftThresh(M-L+Y/mu,lambda/mu);
    
    MmLmS = M-L-S;
    normMmLmS = normL2(MmLmS);
    Y = Y + mu*MmLmS;
    
    %display([' l ' num2str(l) '  obj ' num2str(norm(M-L-S,2))]);
    l = l+1;    
    if ((l > niter) || (normMmLmS < 1e-7*normM))
        break;
    end;        
end;

score = bsxfun(@times,diag(St)',Vt);score=score(:,1:maxout)/sqrt(s(1));
coeff = bsxfun(@times,Ut,diag(St)');coeff=coeff(:,1:maxout);
L = reshape(L,s);
if (centered)
    L = L + repmat(Mm,[1,s(end)]);
end;
S = reshape(S,s);

% display([' Lambda ' num2str(lambda) ', mu ' num2str(mu) ', niter ' num2str(l)]);

end

% soft-thresholding function
function y=SoftThresh(x,p)
t=(abs(x)-p);
y=t.*sign(x);
y(isnan(y))=0;
end    

% hard-thresholding function
function y=HardThresh(x,p)
y=x;
y(x<=p)=0;
y(isnan(y))=0;
end    
