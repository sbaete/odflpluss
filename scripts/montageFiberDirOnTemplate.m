% Steven Baete
% NYU SOM CBI
% August 2016

function montageFiberDirOnTemplate(templatefile,fibfile,fileout,silent,titel,...
       MAX_FIBERS,thres,slisel,faorigthres)

if (nargin < 3 || isempty(fileout)) fileout = []; end;
if (nargin < 4 || isempty(silent)) silent = false; end;
if (nargin < 5 || isempty(titel)) titel = []; end;
if (nargin < 6 || isempty(MAX_FIBERS)) MAX_FIBERS = 5; end;
if (nargin < 7 || isempty(thres)) thres = 0.05; end;
if (nargin < 8 || isempty(slisel)) slisel.x = []; 
    slisel.y = []; slisel.z = []; end;
if (nargin < 9 || isempty(faorigthres)) faorigthres = 0; end;
    
lw = 1;
fs = 14;

%% load fib-file
fib = fib_obj(fibfile);

%% load background-map

templnii = load_untouch_nii(templatefile);
backgimg = double(repmat(templnii.img,[1,1,1,3]));
backgimg = backgimg/max(backgimg(:));
if (~isfield(slisel,'z') || isempty(slisel.z))
	slisel.z = [25:2:56];
end;
if (~isfield(slisel,'x') || isempty(slisel.x)) slisel.x = [1:size(backgimg,1)]; end;
if (~isfield(slisel,'y') || isempty(slisel.y)) slisel.y = [1:size(backgimg,2)]; end;

%% load the dirs
for i = 1:MAX_FIBERS
    dir(i).index = fib.getvar(['index' num2str(i-1)]);
    dir(i).index = dir(i).index(slisel.x,slisel.y,slisel.z);
    dir(i).fa = fib.getvar(['fa' num2str(i-1)]);
    dir(i).faorig = fib.getvar(['Mfa']);
    dir(i).fa = dir(i).fa(slisel.x,slisel.y,slisel.z);
    dir(i).faorig = dir(i).faorig(slisel.x,slisel.y,slisel.z);
    dir(i).sel = (dir(i).fa > thres) & (dir(i).faorig > faorigthres); 
end;

%% locations of voxels
dimension = size(dir(i).index);
ind = 1:prod(dimension);
ind3D = reshape(ind,dimension);
ind2D = montageSB(permute(ind3D,[2,1,3,4]));
ind2D(ind2D==0) = 1e10;

[~,indxy] = sort(ind2D(:));
dim2D = size(ind2D);
mask2D = zeros(dim2D);
mask2D(:) = 1:prod(dim2D);
mask2D = mask2D.*(ind2D<1e10-1);
[x,y] = ind2sub(dim2D,mask2D(:));
loc.x = x(indxy);
loc.y = y(indxy);
loc.z = -1.5*ones(size(loc.x));

% figure;imagesc(ind2D)
% hold on;
% plot(loc.y(1:200),loc.x(1:200),'.-k');
% set(gca,'YDir','normal')

%% filter the directions
for i = 1:MAX_FIBERS
    dir(i).ind = ind(dir(i).sel(:));
    dir(i).index = dir(i).index(dir(i).ind);
    dir(i).fa = dir(i).fa(dir(i).ind);
end;

%% load the odf-vertices directions
load('odf8.mat');

%% plot the background
if (silent)
    figure('Color','k','Position',[0 0 800 800],'Visible','off');
else
    figure('Color','k','Position',[0 0 800 800]);
end;
di = backgimg;
di = squeeze(montageSB(flipdim(permute( ...
    di(slisel.x,slisel.y,slisel.z,:),[2,1,3,4]),1)));
imagesc(di);axis off
view(0,-90)
get(gca,'Position');
set(gca,'YDir','normal')
if (~isempty(titel))
    title(strrep(titel,'_',''),'FontSize',fs,'Color','w');
end

%% plot the dirs
hold on;
for i = 1:MAX_FIBERS
    posx = loc.x(dir(i).ind);
    posy = loc.y(dir(i).ind);
    posz = loc.z(dir(i).ind);
    for j = 1:length(dir(i).ind)
        vert = odf_vertices(:,dir(i).index(j)+1);
        ff = abs([vert]);
        lscale = dir(i).fa(j)/thres*0.05;
        lscale = max(lscale,1);
        line(posy(j) + lscale*[-vert(1) vert(1)],...
              posx(j) - lscale*[-vert(2) vert(2)],...
              posz(j) + lscale*[-vert(3) vert(3)],...
              'LineWidth',lw,'Color',ff);
    end;
end;
daspect([max(daspect)*[1 1] 1])

%% final touches and save the figure
pos = get(gca,'Position');
sc = 0.03;set(gca,'Position',[pos(1)-pos(3)*sc/2,pos(2)-pos(4)*2.25*sc,...
    pos(3)*(1+sc),pos(4)*(1+3*sc)]);
ax = axes('Position',pos);xlim([0 1]);ylim([0 1]);axis off;
n = ceil(sqrt(length(slisel.z)));
n2 = floor(sqrt(length(slisel.z)));
text(0,(1-0.8*(1/n)),'R','Color','w');

if (~isempty(fileout))
    fig = gcf;
    fig.PaperPositionMode = 'auto';
    fig.InvertHardcopy = 'off';
    print('-dpng','-r0',fileout);
end;