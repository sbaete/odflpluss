% Steven Baete
% NYU SOM CBI
% August 2016

function montageOnTemplate(templatefile,img,titel,thres,fileout,silent,...
                    sel,skeleton,legende,kleur)

if (nargin < 3 || isempty(titel)) titel = []; end;
if (nargin < 4 || isempty(thres)) thres = []; end;
if (nargin < 5 || isempty(fileout)) fileout = []; end;
if (nargin < 6 || isempty(silent)) silent = false; end;
if (nargin < 7 ) sel = []; end;
if (nargin < 8 ) skeleton = []; end;
if (nargin < 9 ) legende = []; end;
if (nargin < 10 ) kleur = []; end;

if (numel(titel) < 3)
    titel{3} = '';
end;
fs = 12;

if (iscell(img))
    imgall = img;
    img = imgall{1};
    for i = 2:length(imgall)
        mask = (img.*imgall{i} > 0);
        img(mask(:)) = min(img(mask(:)),imgall{i}(mask(:)));        
        img(~mask(:)) = max(img(~mask(:)),imgall{i}(~mask(:)));
    end;
    if (isempty(kleur))
        fig = figure('Color','k','Visible','off');
        kleur = get(gca,'colororder');
        close(fig);
    end;
else
    imgall = [];
end;

s = size(img);
%% load template
slisel = [17:58];
if (~isempty(sel)),  slisel = sel; end;
slisel(slisel<0) = [];slisel(slisel>size(img,3)) = [];

templnii = load_untouch_nii(templatefile);
templniiimg = double(repmat(templnii.img,[1,1,1,3]));

%% threshold
if (~isempty(thres))
    img(img < thres) = 0;
    for i = 1:length(imgall)
        imgall{i}(imgall{i} < thres) = 0;
    end;
end;

%% extra layer
if (~isempty(skeleton))
    skeleton = cat(4,zeros(s),cat(4,skeleton.*(1-(img>0)),zeros(s)));
    skeleton(skeleton > 0) = 1;
end;

%% reorganize data
if (length(imgall) == 0)
    niifiltimg = cat(4,img,cat(4,zeros(s),zeros(s)));
    niifiltimg(niifiltimg > 0) = 1;
else
    niifiltimg = zeros(s);
    for i = 1:length(imgall)
        imgall{i}(imgall{i} > 0) = 1;
        niifiltimgi = cat(4,imgall{i}*kleur(i,1),...
            cat(4,imgall{i}*kleur(i,2),imgall{i}*kleur(i,3)));
        niifiltimg = niifiltimg + niifiltimgi;
    end;
end;

%% plot
di = templniiimg/max(templniiimg(:)).*repmat(1-(img>0),[1,1,1,3]) + double(niifiltimg);

if (~isempty(skeleton))
    di = min(di + double(skeleton),1);
end;

di = squeeze(montageSB(flip(permute(di(:,:,slisel,:),[2,1,3,4]),1)));
sd = size(di);
di(di > 1) = 1;
if (silent)
    fig = figure('Color','k','Position',[0 0 sd(2) sd(1)],'Visible','off');
else
    fig = figure('Color','k','Position',[0 0 sd(2) sd(1)],'MenuBar','none','Toolbar','none');
end;
imagesc(di);axis equal;%axis off;
if (~isempty(titel))
    if (isempty(thres))
        title(titel,'FontSize',fs,'Color','w');
    else
        titel{3} = [titel{3} ', p < ' num2str(exp(-thres))];
        title(titel,'FontSize',fs,'Color','w');
    end;
end;
%% final touches and save the figure
set(gca,'xtick',[]);set(gca,'xticklabel',[]);
set(gca,'ytick',[]);set(gca,'yticklabel',[]);
pos = get(gca,'Position');
pm = mean(pos(3:4));
set(gca,'Position',[pos(1) pos(2) pm pm]);axis equal;
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
pos = get(gca,'Position');
ax = axes('Position',min(abs(pos),1));
xlim([0 1]);ylim([0 1]);axis off;
n = ceil(sqrt(length(slisel)));
text(0.03,1-1/n,'R','Color','w');
ax2 = axes('Position',[pos(1),pos(4),pos(3),1-pos(4)]);
xlim([0 1]);ylim([0 1]);axis off;
for i = 1:length(legende)
    t = text(0.5,1.5,strrep(legende{i},'_',' '),'Color','w','FontSize',fs-2);
    ex(i) = t.Extent(3);
end;
w = sum(ex+0.05);
for i = 1:length(legende)
    pos = 0.5-w/2+sum(ex(1:(i-1))+0.05);%0.06+(i-1)*(0.95)/length(legende); 
    rectangle('Position',[pos-0.03,1/4,0.02,1/6],...
        'FaceColor',kleur(i,:),'EdgeColor',kleur(i,:));
    text(pos,1/3,strrep(legende{i},'_',' '),'Color','w','FontSize',fs-2);
end;

if (~isempty(fileout))
    fig = gcf;
    fig.PaperPositionMode = 'auto';
    fig.InvertHardcopy = 'off';
    print('-dpng','-r0',fileout);   
end;