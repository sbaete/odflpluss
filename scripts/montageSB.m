% Steven Baete
% NYU SOM CBI
% August 2017

%% Make a montage of images in matrix format
% input is 4D, with the third dimension, the one which will be used in the
% montage
% dimensions higher than 4 will be collapsed on the 4th dimension

function Xi = montageSB(X,varargin)

wat=what;
origpath=wat.path;

if isunix == 1
    slash='/';
    linux = 1;
elseif ispc == 1
    slash='\';
    linux = 0;
else
    error
end;

% process the input arguments
norm = false;
for i=1:2:size(varargin,2)
    if (~ischar(varargin{i}))
        error('montageSB: Please use string-value pairs for input');
    end;
    switch varargin{i}
        case 'norm'
            norm = varargin{i+1};
        otherwise
            error(['montageSB: I did not recognize the input-string ' varargin{i}]);
    end;
end;

s = size(X);
if (length(s) == 2)
    s(3) = 1;
end;
if (length(s) == 3)
    s(4) = 1;
end;
if (length(s) > 4)
    for j=length(s):-1:5
        sn = size(X);
        Xn = zeros([sn(1:(end-2)),sn(end)*sn(end-1)]);
        for i = 1:sn(end)
            for k = 1:sn(end-1)
                switch j
                    case 5
                        Xn(:,:,:,k+(i-1)*sn(end-1)) = X(:,:,:,k,i);
                    case 6
                        Xn(:,:,:,:,k+(i-1)*sn(end-1)) = X(:,:,:,:,k,i);
                    case 7
                        Xn(:,:,:,:,:,k+(i-1)*sn(end-1)) = X(:,:,:,:,:,k,i);
                    otherwise
                        error('montageSB: this dimension was not foreseen!');
                end;
            end;
        end;
        X = Xn;clear Xn;
    end
    s = size(X);
end;
nslice = s(3);

e = ceil(sqrt(nslice));
Xi = zeros(e*s(1),e*s(2),s(3)/nslice,s(4));
for k = 1:s(4)
    for i = 1:(s(3)/nslice)
        offset = (i-1)*nslice;
        temp = [];
        for j = 1:nslice
            xi = ceil((j)/e);
            yi = mod((j)-1,e)+1;
            if norm
                n = max(max(X(:,:,offset+j,k)));
            else
                n = 1;
            end;
            Xi((xi-1)*s(1)+(1:s(1)),(yi-1)*s(2)+(1:s(2)),i,k) = X(:,:,offset+j,k)/n;
        end;
    end;
end;

