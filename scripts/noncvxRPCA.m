% This is the implementation of nonconvex RPCA method in ICDM 2015 paper
% 'Robust PCA via Nonconvex Rank Approximation'
% Zhao Kang, August 2015. Questions? Zhao.Kang@siu.edu;

function [L,S,score,coeff,Mm,muzero] = noncvxRPCA(M,lambda,muzero,centered,maxout,niter)

if (nargin<2 || isempty(lambda))
    lambda = 1/sqrt(min(size(M)));
end;

if (nargin<3 || isempty(muzero))
    muzero = 0.9;
end;

if (nargin<4)
    centered = false;
end;

if (nargin<5)
    maxout = 5;
end;
maxout = min(maxout,size(M,1));

if (nargin<6 || isempty(niter))
   niter = 500;
end;

s = size(M);
M=reshape(M,[prod(s(1:(end-1))),s(end)]);

if (centered)
    Mm = mean(M,2);
    M = M - repmat(Mm,[1,s(end)]);
else
    Mm = [];
end;

scale = median(abs(M(M(:)~=0)));
M = M/scale;

[m,n] = size(M);
% muzero = 2.5;%.9; % the only tuning parameter
% lambda = 1/sqrt(max(size(M))); % default lambda
%lambda = 1e-3;  % model parameter
type = 21;  %different modeling of Sparsity.
rate = 1.1;   %update rate of \mu
gamma = 0.01;     %gamma parameter in the rank approximation
tol = 1e-6;  % stopping criterion

%initializations
S = zeros(m,n);
Y = zeros(m,n);
L = M;
sig = zeros(min(m,n),1); % for DC
mu = muzero;

% figure(1);imagesc([L,S]);hold on;drawnow;pause(1.0);

for ii=1:niter
  D = M-S-Y/mu;
  [L,sig] = DC(D,mu/2,sig,gamma);
  [S] = errorsol(Y,M,L,lambda,mu,type);
  Y = Y + mu*(L-M+S);
  mu = mu * rate;
  
  sigma = norm(M-S-L,'fro');
  RRE = sigma/norm(M,'fro');
  
%   figure(1);imagesc([L,S]);drawnow;
%   figure(2);plot(sig);drawnow;pause(0.5);
%   disp([' ' num2str(ii) '  relative err ',num2str(RRE),' rank ',num2str(rank(L))]);
  
  if RRE < tol
    break;
  end  
end
%rk = rank(L);
% disp(['  relative err ',num2str(RRE),' rank ',num2str(rk)]);

L = L*scale;
S = S*scale;

L = reshape(L,s);
if (centered)
    L = L + repmat(Mm,[1,s(end)]);
end;
S = reshape(S,s);

[coeff,score] = pca(L','NumComponents',maxout);
