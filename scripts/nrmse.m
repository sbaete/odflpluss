
% Steven Baete
% NYU SOM CBI
% Oktober 2017

function r = nrmse(A,B)

r = zeros(size(A,2),size(A,3));
s = size(A);
a = zeros(s(1),1);b = zeros(s(1),1);
for i = 1:size(A,2)
    for j = 1:size(A,3)
        a(:) = A(:,i,j);
        b(:) = B(:,i,j);
        
        a = a-min(a)*0.9999;
        a = a/max(a);
        b = b-min(b)*0.9999;
        b = b/max(b);
        
        a(isnan(a)) = 0;
        b(isnan(b)) = 0;
        
        r(i,j) = sqrt(mean((a - b).^2))/norm(a);
    end;
end;

r(isnan(r)) = 0;
