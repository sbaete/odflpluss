%% L+S decomposition of ODFs of groups of volunteers

% Steven Baete
% NYU SOM CBI
% December 2015

% -----------------------------------------------------------------------
% This method has been published in:
%  Baete, Chen, Lin, Wang, Otazo, Boada. Low Rank plus Sparse Decomposition 
%	of ODFs for Improved Detection of Group-level Differences and 
%	Variable Correlations in White Matter. 
%	Accepted for publication in NeuroImage, 2018.
% -----------------------------------------------------------------------

% Based on the script anaFibGz_odf_wholebrain and derivatives by
% Steven Baete and Jingyun Chen, NYU LMC CBI, Oct 2014

%   Input:
%       fibfilename: cell-structure with the filenames of the fib-files of
%           all cases
%       templatefile: location of template nii(.gz) file used (to save 
%           output in a similar nifti-structure)
%       dmatfile,dconfile: location of design matrix and contrast file
%           using the fsl randomize format
%   Options (in 'keyword','value' -format)
%       method: algorithm for L+S decomposition
%           (default) 'noncvxRPCA', 'L+S_ad', 'PCA'
%       checkQA: just plot some QA-maps to check the input dataset
%           [false(default),true]
%       maskthres: threshold used to make the mask, mask > mean(FA)*maskthres
%           (default) 1.25
%       outfold: folder where to save the output, script will make the
%           folder if necessary

function odf_lps_calc(fibfilename,templatefile,dmatfile,dconfile,varargin)

close all;

checkQA = false;
maskthres = 1.25;
maxpc = 5;
outfold = 'lps';
centered = true;
norma = true;
muopt = [];
rankthr = 0.05;
method = 'noncvxRPCA';
saveregion = false;
region = [];
if (nargin > 4)
    for i=1:2:size(varargin,2)
        if (~ischar(varargin{i}))
            error('odf_lps_calc: Please use string-value pairs for input');
        end;
        switch varargin{i}
            case 'checkQA'
                checkQA = varargin{i+1};                
            case 'maskthres'
                maskthres = varargin{i+1};
            case 'outfold'
                outfold = varargin{i+1};
            case 'method'
                method = varargin{i+1};
            case 'saveregion'
                saveregion = varargin{i+1};
            case 'region'
                region = varargin{i+1};
            otherwise
                error(['odf_lps_calc: I did not recognize the input-string ' varargin{i}]);
        end;
    end;
end;

if (~isempty(outfold))
    if ~(exist(outfold,'dir')==7), mkdir(outfold); end;
end;
ncases = length(fibfilename);

%% Perform some checks
%  Timestamps of the fib.gz files
for i = 1:ncases
    FileInfo = dir([fibfilename{i}]);
    try
        TimeStamp{i} = FileInfo.date;
    catch
        display([' Could not find file ' fibfilename{i}]);
        error([' Could not find file ' fibfilename{i}]);
    end;
end;
for j = 1:length(TimeStamp)
    display([' ' num2str(j) ' - ' num2str(TimeStamp{j}) ' - ' fibfilename{j}]);
end;

%% load the design-matrix and design-contrast file

dmat = readFSLdesign(dmatfile);
dcon = readFSLdesign(dconfile);
tvar = dmat(:,find(dcon(1,:)~=0,1));

%% load all the fib.gz files
for i = 1:ncases
    display([' reading ' num2str(i) '/' num2str(ncases)]);
    if (exist([fibfilename{i}(1:(end-3)) 't'])==2)
        fib{i} = fib_obj([fibfilename{i}(1:(end-3)) 't']);
    else
        fib{i} = fib_obj(fibfilename{i});
    end;
end;

%% load all the fa-maps and make a common mask
for i = 1:ncases
    fat = fib{i}.getvar('fa0');
    if (i == 1)
        faall = zeros([size(fat),ncases]);
        fa = ones(size(fat));
        faavg = ones(size(fat));
        b0avg = ones(size(fat));
    end;
    faall(:,:,:,i) = fat;
    if (fib{i}.isvar('b0'))
        b0 = fib{i}.getvar('b0');
    elseif (fib{i}.isvar('iso'))
        b0 = fib{i}.getvar('iso');
    else
        b0 = zeros(size(fat));
    end;
    fa = (fa ~= 0.00) & (fat ~= 0.00);
    faavg = faavg + fat;
    b0avg = b0avg + b0;
end;
clear fat;
faavg = faavg/ncases;
b0avg = b0avg/ncases;
mask = (fa.*(faavg>(mean(faall(faall(:)>0)*maskthres))))==1;
volsize = numel(mask);
faallt = reshape(faall,[numel(mask),size(faall,4)]);
famean = mean(faallt(mask(:),:),1);
fammean = mean(famean);
famstd = mean(std(faallt(mask(:),:),1));
clear fa faallt;

if (saveregion)
    mask = zeros(size(mask));
    mask(region.xsel,region.ysel,region.zsel) = 1;
end;

% plot some values for a visual check
figure;plot(famean);title('Mean FA/QA-value');
    xlabel('cases');ylabel('FA/QA');
figure;plot(squeeze(max(max(max(faall,[],1),[],2),[],3)));title('Max FA/QA-value');
    xlabel('cases');ylabel('FA/QA');        
figure;imagesc(montageSB(mask));title('Mask');axis off;colormap('Bone');

%% check the QA-map input if desired
if (checkQA)
    figure;imagesc(squeeze(montageSB(squeeze(faall(:,:,floor(end/2),:)))));
        title('FA/QA-maps');axis off;colorbar
    return;
end;

%% save some maps
niitempl = load_untouch_nii(templatefile);

% mask
nii = niitempl;
nii.img = flip(mask,2);
save_untouch_nii(nii,[outfold filesep 'mask.nii.gz']);
% average fa
nii = niitempl;
nii.img = flip(faavg,2);
save_untouch_nii(nii,[outfold filesep 'fa_avg.nii.gz']);
% average b0
nii = niitempl;
nii.img = flip(b0avg,2);
save_untouch_nii(nii,[outfold filesep 'b0_avg.nii.gz']);

%% prepare matrices to save the results
nvoxel = (sum(mask(:)));
vsize = 321;

npc = zeros(1,nvoxel,'single');
pall = zeros(nvoxel,maxpc,'double');
ts = zeros(nvoxel,maxpc,'single');
pc = zeros(nvoxel,vsize,maxpc,'single');
scores = zeros(nvoxel,ncases,maxpc,'single');
rankL = zeros(1,nvoxel,'double');
cardS = zeros(1,nvoxel,'double');
oversamplingM = zeros(1,nvoxel,'double');

%% loop over the voxels in blocks
maxsize = 1.33*1e10;
bytesize = 4;
blocksize = 2*min(floor(maxsize/(ncases*vsize*bytesize)),nvoxel);

tic;
for loop = 1:blocksize:nvoxel
    %% initialize loopmatrices
    loopind = loop - 1 + (1:blocksize);
    loopind = loopind(loopind <= nvoxel);
    looplength = length(loopind);
        
    odf = zeros(ncases,vsize,length(loopind),'single');
    sm = size(odf);

    disp([' block begin ind ' num2str(loop) ...
       ' (blocksize: ' num2str(blocksize) ', nvoxel: ' num2str(nvoxel) ')']);
   
	%% load the odfs for this block
    for i = 1:ncases
        disp(['   load [' num2str(i) '/' num2str(ncases) ']']);
        odft = single(fib{i}.getvar('odf'));
        odft = reshape(odft,[sm(2),volsize]);
        odft = odft(:,mask(:) > 0);
        odft = odft - repmat(min(odft,[],1),[size(odft,1),1]);
        odf(i,:,:) = odft(:,loopind);
    end;    
    clear odft;
    display(['   loading finished']);
    
    %% in case we just want to save a region for further analysis
    if (saveregion)
        save([outfold filesep 'odf_lps_mat_region.mat'],...
        'odf','mask','fibfilename','ncases','vsize',...
        'faavg','b0avg','tvar','maxpc','region','-v7.3');
        return
    end;
    
    %% run the L+S decomposition
    subblocksize = 10000;
    
    for subloop = 1:subblocksize:looplength
        subloopind = loop - 1 + subloop -1 + (1:subblocksize);
        subloopind = subloopind(subloopind <= (loop-1 + looplength));
        sublooplength = length(subloopind);
        blockind = subloop -1 + (1:subblocksize);
        blockind = blockind(blockind <= looplength);

        odfsubloop = odf(:,:,blockind);
        odfsubloop(isnan(odfsubloop)) = 0;
        odfsubloop(isinf(odfsubloop)) = 0;

        npcloop = zeros(1,sublooplength,'single');
        tsloop = zeros(sublooplength,maxpc,'single');
        pallloop = zeros(sublooplength,maxpc,'single');
        pcloop = zeros(sublooplength,vsize,maxpc,'single');
        scoresloop = zeros(sublooplength,ncases,maxpc,'single');        
        rankLloop = zeros(1,sublooplength,'single');
        cardSloop = zeros(1,sublooplength,'single');
        oversamplingMloop = zeros(1,sublooplength,'single');

        t = toc;
        disp(['   processing [' num2str(subloopind(1)) '/' ...
               num2str(length(loopind)) '/' num2str(nvoxel) '] ' ...
               num2str(t) ' s']);

        if (isempty(gcp('nocreate')))
           parpool(12);
        end;
        
        parfor jsel = 1:length(subloopind) %parfor
            score = [];coeff = [];LR = [];SR = [];
            % select the odfs
            odfp = odfsubloop(:,:,jsel);
            odfp = odfp';
            
            % calculate oversampling
            [~,s,~] = svdecon(odfp);rankMloop = rank(odfp,s(1)*rankthr);
            cardMloop = sum(abs(odfp(:))>(max(abs(odfp(:)))*rankthr));
            oversamplingMloop(jsel) = cardMloop...
                /((sum(size(odfp)-rankMloop)*rankMloop));
            
            % L+S decomposition or PCA
            switch(method)
                case 'PCA'
                    score = [];coeff = [];
                    LR = odfp;SR = [];
                    rankLloop(jsel) = 0;
                    cardSloop(jsel) = 0;
                case 'L+S_ad'
                    [LR,SR,score,coeff] = lps_ad(odfp,[],muopt,centered);
                case 'noncvxRPCA'
                    [LR,SR,score,coeff] = noncvxRPCA(odfp,[],muopt,centered);
                otherwise
                    error('odf_lps_calc: invalid method');
            end;            
            LR(isnan(LR)) = 0;LR(isinf(LR)) = 0;
            if (~isempty(SR))                
                [~,s,~] = svdecon(LR);
                rankLloop(jsel) = rank(LR,s(1)*rankthr);
                SR(isnan(SR)) = 0;SR(isinf(SR)) = 0;
                cardSloop(jsel) = sum(abs(SR(:))>(max(abs(odfp(:)))*rankthr));
            end;
            [pt,statst,pct,score] = ...
                pca_odf(LR,tvar,score,coeff,[],norma,centered);
               
            % --------------- save the data
            npcloop(jsel) = sum(sum(abs(score),1)>0);
            tsloop(jsel,:)=statst(:);
            pallloop(jsel,:)=pt(:);
            pcloop(jsel,:,:) = pct(:,:);     
            scoresloop(jsel,:,:) = score(:,:);
        end;
        
        % todo from here
        %% copy data in the larger loop variables
        npc(subloopind) = npcloop;
        ts(subloopind,:) = tsloop;
        pall(subloopind,:) = pallloop;
        pc(subloopind,:,:) = pcloop;
        scores(subloopind,:,:) = scoresloop;
        rankL(subloopind) = rankLloop/min(ncases,vsize);
        cardS(subloopind) = cardSloop/prod(ncases*vsize);
        oversamplingM(subloopind) = oversamplingMloop;
    end;
end;
clear odf;

%% save the output
save([outfold filesep 'odf_lps_mat.mat'],...
    'npc','ts','pall','pc','scores',...
    'mask','fibfilename','ncases','vsize',...
    'faavg','b0avg','tvar','maxpc',...
    'rankL','cardS','oversamplingM','-v7.3');

% map of significance
for i = 1:maxpc
    tsout=zeros(size(mask));
    tsout(mask==1)=-log(pall(:,i));
    nii = niitempl;
    nii.img = flip(tsout,2);
    save_untouch_nii(nii,[outfold filesep 'pmap_' num2str(i) '.nii.gz']);
end;

% map of significance, min
tsout=zeros(size(mask));
tsout(mask==1)=-log(min(pall(:,:),[],2));
nii = niitempl;
nii.img = flip(tsout,2);
save_untouch_nii(nii,[outfold filesep 'pmap.nii.gz']);

% maps of parameters
paramout = {'npc';'rankL';'cardS';'oversamplingM';};
for pp = 1:length(paramout)
    tsout=zeros(size(mask));
    eval(['tsout(mask==1)=abs(' paramout{pp} '(:));']);
    nii = niitempl;
    nii.img = flip(tsout,2);
    save_untouch_nii(nii,[outfold filesep paramout{pp} '.nii.gz']);
end;

display([' Mean Norm Rank of L : ' num2str(mean(rankL)) ...
        '  std ' num2str(std(rankL))]);
display([' Mean Norm Card of S : ' num2str(mean(cardS)) ...
        '  std ' num2str(std(cardS))]);
display([' Oversampling of M   : ' num2str(mean(oversamplingM)) ...
    '  std ' num2str(std(oversamplingM))]);


