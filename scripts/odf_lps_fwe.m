%% L+S decomposition of ODFs of groups of volunteers
%  FWE-correction of the results using FSL randomize

% Steven Baete
% NYU SOM CBI
% April 2016

% -----------------------------------------------------------------------
% This method has been published in:
%  Baete, Chen, Lin, Wang, Otazo, Boada. Low Rank plus Sparse Decomposition 
%	of ODFs for Improved Detection of Group-level Differences and 
%	Variable Correlations in White Matter. 
%	Accepted for publication in NeuroImage, 2018.
% -----------------------------------------------------------------------

%   Input:
%       lpsfold: folder with L+S-decomposition results
%       fslfold: folder with the fsl-binaries, i.e. where we can find 
%           FSL randomise. e.g. '/usr/lib/fsl/5.0'
%       dmatfile,dconfile: location of design matrix and contrast file
%           using the fsl randomize format
%   Options (in 'keyword','value' -format)
%       permutations: number of permutations to run for FSL randomise
%           default: 1000

function odf_lps_fwe(lpsfold,fslfold,dmatfile,dconfile,varargin)

permutations = 1000;
if (nargin > 4)
    for i=1:2:size(varargin,2)
        if (~ischar(varargin{i}))
            error('odf_lps_fwe: Please use string-value pairs for input');
        end;
        switch varargin{i}
            case 'permutations'
                permutations = varargin{i+1}; 
            otherwise
                error(['odf_lps_fwe: I did not recognize the input-string ' varargin{i}]);
        end;
    end;
end;

display(['   	   data located in ' lpsfold]);
display(['   	    fsl located in ' fslfold]);

%% which randomize-script to use
if (exist([fslfold filesep 'randomise_parallel_sb'],'file') && permutations > 2000)
    % this script has minor changes relative to the version included in FSL
    % contact me for more info
    randstr = [fslfold filesep 'randomise_parallel_sb'];
else
    randstr = [fslfold filesep 'randomise'];
end;

%% load the odf_lps_calc-results

load([lpsfold filesep 'odf_lps_mat.mat'],'scores','mask','maxpc');
nvol = size(scores,2);

%% loop over the principal components

for k = 1:maxpc
    %% reorganize the pc-scores
    mmask = repmat(mask,[1,1,1,nvol]);
    tsout=zeros(size(mmask));
    tmp = scores(:,:,k);
    tsout(mmask==1)= tmp;
    tsout = flipdim(tsout,2);    
    
    %% where to save the results
    tempfold = [lpsfold filesep 'fwe_pc' num2str(k) filesep];
    if (~(exist(tempfold,'dir')))
        mkdir(tempfold);
    end;
        
    %% save the mask as a nifti
    nii = load_untouch_nii([lpsfold filesep 'pmap.nii.gz']);
    nii.img = flipdim(mask,2);
    save_untouch_nii(nii,[tempfold 'mask.nii']);
    
    %% save the data as a nifti
    nii.hdr.dime.datatype = 64;
    nii.hdr.dime.bitpix = int16(64);
    nii.img = double(nii.img);
    nii.img = tsout/max(tsout(:))*10000;
    nii.hdr.dime.dim(1) = 4;
    nii.hdr.dime.dim(5) = nvol;
    save_untouch_nii(nii,[tempfold 'datain.nii']);
    
    %% run FSL randomize
    system([randstr ' -i ' tempfold 'datain.nii '...
         '-o ' tempfold 'data ' ...
         '-d ' dmatfile ' ' ...
         '-t ' dconfile ' ' ...
         '-m ' tempfold 'mask.nii ' ...
         '-n ' num2str(permutations) ' -T -R']);
    system(['rm ' tempfold 'datain.nii']);
end;
