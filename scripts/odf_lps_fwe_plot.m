%% L+S decomposition of ODFs of groups of volunteers
%  FWE-correction of the results using FSL randomize

% Steven Baete
% NYU SOM CBI
% April 2016

% -----------------------------------------------------------------------
% This method has been published in:
%  Baete, Chen, Lin, Wang, Otazo, Boada. Low Rank plus Sparse Decomposition 
%	of ODFs for Improved Detection of Group-level Differences and 
%	Variable Correlations in White Matter. 
%	Accepted for publication in NeuroImage, 2018.
% -----------------------------------------------------------------------

%   Input:
%       lpsfold: folder with L+S-decomposition results
%       templatefile: location of template nii(.gz) file used (to save 
%           output in a similar nifti-structure)
%       dmatfile,dconfile: location of design matrix and contrast file
%           using the fsl randomize format
%   Options (in 'keyword','value' -format)
%       pthres: threshold for the p-value plots
%           default: 0.05
%       silent: display plots or generate in the background
%           default: true
%       tests: cell-structure with an explanation for each test

function odf_lps_fwe_plot(lpsfold,templatefile,dmatfile,dconfile,varargin)

pthres = 0.05;
silent = true;
tests = [];
if (nargin > 4)
    for i=1:2:size(varargin,2)
        if (~ischar(varargin{i}))
            error('odf_lps_fwe: Please use string-value pairs for input');
        end;
        switch varargin{i}  
            case 'pthres'
                pthres = varargin{i+1};
            case 'silent'
                silent = varargin{i+1};
            case 'tests'
                tests = varargin{i+1};
            otherwise
                error(['odf_lps_fwe: I did not recognize the input-string ' varargin{i}]);
        end;
    end;
end;

display(['   	   data located in ' lpsfold]);

%% how many statistical tests are we performing?
load([lpsfold filesep 'odf_lps_mat.mat'],'maxpc');
dcon = readFSLdesign(dconfile);
ntest = size(dcon,1);
 
if (isempty(tests))
    tests = cell(ntest,1);
    for j = 1:ntest
        tests{j} = ['test ' num2str(j)];
    end;
end;

%% loop over the statistical tests
for j = 1:ntest
    clear nii;
    
    %% plot for each pc seperately 
    for k = 1:maxpc
        tempfold = [lpsfold filesep 'fwe_pc' num2str(k) filesep];
        niifname = [tempfold 'data_tfce_corrp_tstat' num2str(j) '.nii.gz'];
        if (~exist(niifname,'file'))
            niifname = [tempfold 'data_tfce_corrp_tstat' num2str(j) '.nii'];
            if (~exist(niifname,'file'))
                display('Did not find all necessary nii-files.');
                break;
            end;
        end;
        dnii = load_untouch_nii(niifname);
        dnii.img = -log(1-dnii.img);
        nii{k} = dnii;
        save_untouch_nii(dnii,[tempfold 'p_test' num2str(j) '.nii.gz']);
        
        titel = {strrep(niifname,'_',' ');...
            ['pc ' num2str(k)];...
            tests{j}};
        montageOnTemplate(templatefile,dnii.img,titel,...
            -log(pthres),...
            [tempfold 'p_test' num2str(j) '_thres' num2str(pthres) '.png'],silent);
    end;
    
    %% plot the smallest p-value for all pc
    dnii = nii{1};
    for k = 2:length(nii)
        dnii.img = max(dnii.img,nii{k}.img); % -log-shape, so take the max
    end;
    save_untouch_nii(dnii,[lpsfold filesep 'p_test' num2str(j) '.nii.gz']);

    titel = {strrep(lpsfold,'_',' ');...
        '';...
        strrep(tests{j},'_',' ')};
    montageOnTemplate(templatefile,dnii.img,titel,...
        -log(pthres),...
        [lpsfold filesep 'p_test' num2str(j) '_thres' num2str(pthres) '.png'],silent);
    
    %% plot thresholded and filtered p-value maps
    niifilt = dnii;
    niifilt.img = exp(-dnii.img);
    niifilt.img(niifilt.img > pthres) = 0;
    
    %% filter out the one-pixel objects    
    BW = (niifilt.img > 0);
    CC = bwconncomp(BW);
    numPixels = cellfun(@numel,CC.PixelIdxList);
        
    for jl = 1:length(numPixels)
        if (numPixels(jl) < 5)
            BW(CC.PixelIdxList{jl}) = 0;
        end;
    end;
    niifilt.img = niifilt.img.*BW;
           
    %% save on MNI
    
    titel = {strrep(lpsfold,'_',' ');...
        '';...
        strrep(tests{j},'_',' ')};
    montageOnTemplate(templatefile,niifilt.img,titel,...
            [],...
            [lpsfold filesep 'p_test' num2str(j) '_thres' num2str(pthres) '_filt.png'], ...
            silent);
    
end;
