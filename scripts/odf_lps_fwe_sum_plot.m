%% L+S decomposition of ODFs: plots of p-maps
%  plot of multiple FWE corrected p-maps of one or multiple lps-analysis
%  on a single template map

% Steven Baete
% NYU SOM CBI
% April 2016

% lpsfold, dmatfile, dconfile are cell structures with one entry per map to
% be added to the plot

%   Input:
%       lpsfold: cell-structure with the folders with L+S-decomposition results
%       templatefile: location of template nii(.gz) file used (to save 
%           output in a similar nifti-structure)
%       dmatfile,dconfile: cell-structures with the location of 
%           design matrix and contrast file using the fsl randomize format
%           for the contrasts
%   Options (in 'keyword','value' -format)
%       pthres: array of threshold for the p-value plots
%           default: 0.05
%       silent: display plots or generate in the background
%           default: true
%       tests: array with the numbers of the tests of to add to the map
%       filter: filter out small areas from the p-map
%       slisel: which slices of the template to use
%       legend: legend entries
%       title:  title to use on the plot
%       fileout: location to save the plot
%       overlap: when two or more p-maps are added, some can overlap. For
%           each row (j,k) in overlap, a separate map will be made with the overlap
%           of entries j and k in the lpsfold-cell structure. Don't forget to
%           add extra legend entries.

function odf_lps_fwe_sum_plot(lpsfold,templatefile,dmatfile,dconfile,varargin)

pthres = [];
silent = true;
tests = [];
filter = true;
inptitel = [];
inplegende = [];
slisel = [];
fileout = [];
overlap = [];
kleur = [];
if (nargin > 4)
    for i=1:2:size(varargin,2)
        if (~ischar(varargin{i}))
            error('odf_lps_fwe_sum_plot: Please use string-value pairs for input');
        end;
        switch varargin{i}  
            case 'pthres'
                pthres = varargin{i+1};
            case 'silent'
                silent = varargin{i+1};
            case 'tests'
                tests = varargin{i+1};
            case 'filter'
                filter = varargin{i+1};
            case 'title'
                inptitel = varargin{i+1};
            case 'legend'
                inplegende = varargin{i+1};
            case 'slisel'
                slisel = varargin{i+1};
            case 'fileout'
                fileout = varargin{i+1};
            case 'overlap'
                overlap = varargin{i+1};
            case 'color'
                kleur = varargin{i+1};
            otherwise
                error(['odf_lps_fwe_sum_plot: I did not recognize the input-string ' varargin{i}]);
        end;
    end;
end;

if (isempty(pthres)) pthres = ones(1,length(lpsfold)); end;
if (isempty(tests)) tests = ones(size(pthres)); end;

deftitel = [];
for b = 1:length(lpsfold)
    clear nii;

    display(['   	   data located in ' lpsfold{b}]);
    %% load the p-map
    niifname = [lpsfold{b} filesep 'p_test' num2str(tests(b)) '.nii.gz'];
    if (~exist(niifname,'file'))
         % remove *.gz
        if (~exist(niifname(1:(end-4)),'file'))
            display(['Did not find all necessary nii-files. Missing ' niifname]);
            break;
        end;
    end;
    dnii = load_untouch_nii(niifname);

    %% plot thresholded and filtered p-value maps
    niifilt = dnii;
    niifilt.img = exp(-dnii.img);
    niifilt.img(niifilt.img > pthres(b)) = 0;
    
    %% filter out the one-pixel objects    
    if (filter) 
        BW = (niifilt.img > 0);
        CC = bwconncomp(BW);
        numPixels = cellfun(@numel,CC.PixelIdxList);

        for jl = 1:length(numPixels)
            if (numPixels(jl) < 5)
                BW(CC.PixelIdxList{jl}) = 0;
            end;
        end;
        pmap{b} = niifilt.img.*BW;
    else
        pmap{b} = niifilt.img;
    end;
    
    deftitel = [deftitel lpsfold{b} ' - '];
    deflegende{b} = [lpsfold{b} ' test ' num2str(tests(b)) ' p < ' num2str(pthres(b))];
end;

%% overlap of  maps
if (~isempty(overlap))
    for j = 1:size(overlap,1)
        pmap{length(lpsfold)+j} = pmap{overlap(j,1)}.*pmap{overlap(j,2)} > 0;
        pmap{overlap(j,1)} = pmap{overlap(j,1)}.*(1-pmap{length(lpsfold)+j}) > 0;
        pmap{overlap(j,2)} = pmap{overlap(j,2)}.*(1-pmap{length(lpsfold)+j}) > 0;
        deflegende{length(lpsfold)+j} = ...
            ['overlap ' lpsfold{overlap(j,1)} ' ' lpsfold{overlap(j,2)}];
    end;
end;

%% save on MNI    

if (isempty(inptitel)),    titel = {strrep(deftitel(1:(end-3)),'_',' ');'';''};
else,     titel = inptitel;      end

if (isempty(inplegende)),    legende = deflegende;
else,     legende = inplegende;      end

montageOnTemplate(templatefile,pmap,titel,[],...
        fileout, ...
        silent,slisel,[],legende,kleur);
    