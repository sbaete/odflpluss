%% L+S paramsearch

% Steven Baete
% NYU SOM CBI
% Oktober 2017

% -----------------------------------------------------------------------
% This method has been published in:
%  Baete, Chen, Lin, Wang, Otazo, Boada. Low Rank plus Sparse Decomposition 
%	of ODFs for Improved Detection of Group-level Differences and 
%	Variable Correlations in White Matter. 
%	Accepted for publication in NeuroImage, 2018.
% -----------------------------------------------------------------------

%   Input:
%       lpsfold: folder with L+S-decomposition results
%   Options (in 'keyword','value' -format)
%       lambda: range of lambda to investigate
%       mu: range of my to investigate

function odf_lps_paramsearch(lpsfold,varargin)

close all;

centered = true;
norma = true;
lambda = [];
mu = [];

if (nargin > 1)
    for i=1:2:size(varargin,2)
        if (~ischar(varargin{i}))
            error('odf_lps_paramsearch: Please use string-value pairs for input');
        end;
        switch varargin{i}
            case 'lambda'
                lambda = varargin{i+1};                
            case 'mu'
                mu = varargin{i+1};
            otherwise
                error(['odf_lps_paramsearch: I did not recognize the input-string ' varargin{i}]);
        end;
    end;
end;

%% load input data for the paramsearch
display(['   	   data located in ' lpsfold]);
load([lpsfold filesep 'odf_lps_mat_region.mat']);
nodf = size(odf,3);
vsize = size(odf,2);
ncases = size(odf,1);

%% ranges of parameters to cover

groups = [ones(1,ncases),2*ones(1,ncases)];
[Y,X,Z] = meshgrid(region.xsel,region.ysel,region.zsel);

if (isempty(lambda))
    lambdasugg = 1/sqrt(max(2*ncases,vsize));
    order = floor(log(lambdasugg)/log(10));
    lambda = [0,10^(order-1),5*10^(order-1),10^(order),5*10^(order), ...
        10^(order+1),5*10^(order+1),10^(order+2)];
end;
if (isempty(mu))
    mu = [0.01,0.5:0.2:1.3,2,5];
end;

%% set up the matrices
nmi = 2;
nvoxel = (nodf+1)*nodf/2;
p = zeros(length(lambda)*length(mu),nmi,nvoxel,'double');
ts = zeros(length(lambda)*length(mu),nmi,nvoxel,'double');
dist = zeros(1,nvoxel,'single');
nr = zeros(1,nvoxel,'single');
kl = zeros(1,nvoxel,'single');
js = zeros(1,nvoxel,'single');
Ma = zeros(length(lambda)*length(mu),floor(nodf/10),vsize,2*ncases,'single');
La = zeros(length(lambda)*length(mu),floor(nodf/10),vsize,2*ncases,'single');
Sa = zeros(length(lambda)*length(mu),floor(nodf/10),vsize,2*ncases,'single');

%% calculate the differences between the odfs in the different voxels

display([' calculating voxel differences']);

l = 0;
for i = 1:nodf
    for j = 1:i
        l = l + 1;
        odf1 = odf(:,:,i);
        odf2 = odf(:,:,j);

        % distance measures
        dist(l) = sqrt((X(i)-X(j)).^2 + (Y(i)-Y(j)).^2 + (Z(i)-Z(j)).^2); 
        nr(l) = nrmse(mean(odf1',2),mean(odf2',2));
        kl(l) = kld(mean(odf1',2),mean(odf2',2));
        js(l) = jsd(mean(odf1',2),mean(odf2',2));
    end;
end;

%% run the statistical tests

odf(isnan(odf(:))) = 0;
odf(isinf(odf(:))) = 0;

display([' looping over options for lambda and mu ']);

delete(gcp('nocreate'));
parpool(10);
parfor ii = 1:(length(lambda)*length(mu))
    il = rem(ii-1,length(lambda))+1;
    jm = ceil(ii/length(lambda));
    %for jm = 1%:length(mu)
        pt = zeros(nmi,nvoxel,'single');
        tst = zeros(nmi,nvoxel,'single');
        Mat = zeros(floor(nodf/10),vsize,2*ncases,'single');
        Lat = zeros(floor(nodf/10),vsize,2*ncases,'single');
        Sat = zeros(floor(nodf/10),vsize,2*ncases,'single');
        
        display([' lambda ' num2str(lambda(il)) '    mu ' num2str(mu(jm))]);
        
        l = 0;
        for i = 1:nodf
            for j = 1:i
                l = l + 1;

                % select the odfs
                odf1 = odf(:,:,i);
                odf2 = odf(:,:,j);
                odfp = [odf1;odf2];
                odfp = odfp';
                
                % PCA
                [ptt,statst,~,~] = ...
                    pca_odf(odfp,groups,[],[],[],norma,centered);
                pt(1,l)=ptt(1);tst(1,l)=statst(1);
                
                % noncvxRPCA
                [LR,SR,score,coeff] = noncvxRPCA(odfp,lambda(il),mu(jm),centered);
                LR(isnan(LR)) = 0;LR(isinf(LR)) = 0;
                [ptt,statst,~,~] = ...
                    pca_odf(LR,groups,score,coeff,[],norma,centered);
                [~,pc] = min(ptt);if (isempty(pc)), pc = 1; end;
                pt(2,l)=ptt(pc);tst(2,l)=statst(pc);
                
                % save some M = L+S pairs for later analysis
                if (j == 1 && rem((i+1),10) == 1)
                    Mat(floor(i/10),:,:) = odfp;
                    Lat(floor(i/10),:,:) = LR;
                    Sat(floor(i/10),:,:) = SR;
                end;
            end;
        end;

        p(ii,:,:) = pt;
        ts(ii,:,:) = tst;
        Ma(ii,:,:,:) = Mat;
        La(ii,:,:,:) = Lat;
        Sa(ii,:,:,:) = Sat;
%     end;
end;

%% save the results for display

save([lpsfold filesep 'odf_paramsearch.mat'],...
    'p','ts','dist','nr','kl','js','lambda','mu',...
    'Ma','La','Sa','region','groups','nodf');

display([' L+S paramsearch done']);
