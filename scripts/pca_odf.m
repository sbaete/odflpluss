% Steven Baete
% NYU SOM CBI/CAIIR
% Oktober 2015

function [p,stats,pc,score] = ...
    pca_odf(ODF,tvar,score,coeff,maxout,norma,centered)

if (nargin < 3), score = []; end;
if (nargin < 4), coeff = []; end;
if (nargin < 5 || isempty(maxout)), maxout = 5; end;
if (nargin < 6 || isempty(norma)), norma = false; end;
if (nargin < 7 || isempty(centered)), centered = false; end;

if (~isempty(score)) maxout = min(maxout,size(score,2)); end;

if (~isempty(ODF) && (isempty(score) || isempty(coeff)))
    if (centered)
        Mm = mean(ODF,2);
        ODF = ODF - repmat(Mm,[1,size(ODF,2)]);
    end;
    [coeff,score] = ...
        pca(ODF','NumComponents',maxout);
end;

% normalize the principal components
if (norma==0)
     for j = 1:min(size(score,2),maxout)
        mcoeff = max(coeff(:,j));
        coeff(:,j) = coeff(:,j)/mcoeff;
        score(:,j) = score(:,j)*mcoeff;
     end;
end;

% normalize the scores
if (norma==1)
    for j = 1:min(size(score,2),maxout)
        mscore = 2*mean(abs(score(:,j)));
        coeff(:,j) = coeff(:,j)*mscore;
        score(:,j) = score(:,j)/mscore;
    end;
end; 

% test groups
if (length(unique(tvar)) > 2)
    % we are checking against a variable, e.g. age or bmi
    variable = tvar;
    variable = variable - mean(variable);
    varstd = 2*std(variable);
else
    variable = [];
    if (min(unique(tvar)) ~= 1)
        tmp = unique(tvar);
        tvar(tvar == tmp(1)) = 1;
        tvar(tvar == tmp(2)) = 2;
    end;
end;

score = double(score);
p = ones(1,maxout);stats = zeros(1,maxout);
pc = zeros(size(coeff,1),maxout);
for j = 1:maxout
    if (isempty(variable))
        [~,pt,~,statst] = ttest2(score(tvar == 1,j),score(tvar == 2,j),'Alpha',0.05);
        p(j) = pt;stats(j) = statst.tstat;
        pc(:,j) = (coeff(:,j))*(nanmean(score(tvar == 2,j)) ...
            - nanmean(score(tvar == 1,j)));
    else
        [r,pt] = corr(score(:,j),variable); 
        p(j) = pt;stats(j) = r;
        pc(:,j) = (coeff(:,j))*(score(:,j)'/variable')*varstd;
    end;   
end;
