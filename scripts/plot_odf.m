% Steven Baete
% NYU SOM CBI

function plot_odf(odf,odf_vertices,odf_faces,odf_loc,newfig,varargin)

try
    odf_loc;
    if (length(odf_loc) == 0)
        odf_loc = zeros(size(odf,2),3);
        s = ceil(sqrt(size(odf,2)));
        [X,Y] = meshgrid(1:s,1:s);
        odf_loc(:,1) = X(1:size(odf,2));
        odf_loc(:,2) = Y(1:size(odf,2));
    end;
catch
    odf_loc = zeros(size(odf,2),3);
    s = ceil(sqrt(size(odf,2)));
    [X,Y] = meshgrid(1:s,1:s);
    odf_loc(:,1) = X(1:size(odf,2));
    odf_loc(:,2) = Y(1:size(odf,2));
end;

try
    newfig;
catch
    newfig = true;
end;

onFA = false;
plotZoom = false;
directions = false;
fa = [];
trans = -1;
fcolor = 'interp';fcset = false;
fcoloroutline = [0,0,0];
lw = 1.5;
inter = false;
noodf = false;
xzplane = 0;
notnorm = false;
scale = 1;
swapdir = [1,2,3];
for i=1:2:size(varargin,2)
    if (~ischar(varargin{i}))
        error('plot_odf: Please use string-value pairs for input');
    end;
    switch varargin{i}
        case 'onFA'
            onFA = varargin{i+1};
        case 'plotZoom'
            plotZoom = varargin{i+1};
        case 'directions'
            directions = varargin{i+1};
        case 'fa'
            fa = varargin{i+1};
	    case 'Transparency'
            trans = varargin{i+1};
        case 'FaceColor'
            fcolor = varargin{i+1};
            fcset = true;
        case 'FaceColorOutline'
            fcoloroutline = varargin{i+1};
        case 'LineWidth'
            lw = varargin{i+1};
        case 'Outline'
            inter = varargin{i+1};
        case 'noodf'
            noodf = varargin{i+1};
        case 'xzplane'
            xzplane = varargin{i+1};
        case 'NoNorm'
            notnorm = varargin{i+1};
        case 'Scale'
            scale = varargin{i+1};
        case 'Swapdir'
            swapdir = varargin{i+1};
        otherwise
            error(['plot_odf: I did not recognize the input-string ' varargin{i}]);
    end;
end;

if newfig
    figure;hold on;%('renderer','opengl','color','w');
end;

% check the odf, do we need to re-organize the data
s = size(odf);
if (length(s) == 4)
    odf = odf(:,:,ceil(s(3)/2),:);
    s = size(odf);
    if (sum(odf_loc(:)) == 0);
        [X,Y,Z]=meshgrid(1:s(1),s(2):-1:1,1:s(3));
        odf_loc = [X(:),Y(:),Z(:)];
    end;
    odf = permute(odf,[4,1,2,3]);
    odf = reshape(odf,[s(4),s(1)*s(2)*s(3)]);
end;

no = ([length(unique(odf_loc(:,1))),length(unique(odf_loc(:,2))),length(unique(odf_loc(:,3)))] == 1);
if (sum(no) == 3)
    no = [0,0,1];
end;
if (xzplane)
    no = [0,1,0];
end;
no = double(no);
colors=get(gca,'ColorOrder');

si = [-1,1,1];
if (~directions)
    n = size(odf_vertices,2)/size(odf,1);
    for i = 1:size(odf,2)
        if (notnorm)
            odf_points = (odf_vertices(swapdir,:).*repmat(odf(:,i),[n 3])');
            cdat = zeros([size(odf_points,2),3]);
            cdat((odf(:,i) < 0),:) = repmat(colors(2,:),[sum(odf(:,i) < 0),1]);
            cdat((odf(:,i) >= 0),:) = repmat(colors(1,:),[sum(odf(:,i) >= 0),1]);
            cdat2(:,1,:) = cdat;
        else
            odf_points = (odf_vertices(swapdir,:).*repmat(odf(:,i)-min(odf(:,i)),[n 3])');
        end;
        for kk = 1:3
            odf_points(kk,:) = si(kk)*odf_points(kk,:);
        end;
        try
            polygon.vertices = odf_points'*scale + repmat(odf_loc(i,:),[size(odf_points,2),1]);
            polygon.faces = odf_faces'+1;
            polygon.facevertexcdata = min(abs(odf_vertices.^2)',1);
            %figure;
            if (~noodf)
                p = patch(polygon);
                if (notnorm)
                    set(p,'FaceColor',fcolor,'EdgeColor','none');
                    set(p,'CData',cdat2,'EdgeColor','none');
                else
                    set(p,'FaceColor',fcolor,'EdgeColor','none');
                end;
                if (trans > 0)
                    alpha(p,trans);
                end;
            end
            if (inter)
                lin = intersectPlaneSurf(polygon,odf_loc(i,:),no);
                for i = 1:length(lin)
                    p = plot3(lin{i}(1,:),lin{i}(2,:),lin{i}(3,:),'LineWidth',lw,'Color',fcoloroutline);
                end;
            end;        
        end;
    % if newfig
        daspect([1 1 1])
        if (~onFA && ~inter)
            view(3); axis tight
    % end;
            if (i == 1)
                camlight
    %             material dull
    %             shading interp
            end;
            lighting gouraud
        end;
    end
else
    if (isempty(fa))
        fa = ones(size(odf))*0.5;
    else
        fa = fa*2;
    end;
    for i = 1:size(odf,2)
        for j = 1:size(odf,1)
            if (odf(j,i) > 0)
                for k = 1:3
                    xyz(k,:) = odf_loc(i,k) + si(k)*fa(j,i)*[-odf_vertices(k,odf(j,i)+1),odf_vertices(k,odf(j,i)+1)];
                end;
                if (~fcset)
                    ff = abs([odf_vertices(:,odf(j,i)+1)]);
                else
                    ff = fcolor;
                end;
                plot3(xyz(1,:),xyz(2,:),xyz(3,:),'-','LineWidth',lw,'Color',ff);
            end;
        end;
    end;
end;

if (plotZoom)
    xl = get(gca,'XLim');yl = get(gca,'YLim');
    xlm = floor(mean(xl));ylm = floor(mean(yl));
    plot3([xlm-0.5,xlm-0.5,xlm+0.5,xlm+0.5,xlm-0.5],[ylm-0.5,ylm+0.5,ylm+0.5,ylm-0.5,ylm-0.5],[1]*ones(1,5),'-w','LineWidth',1);
    rat = 3/11;
    ax1 = gca;
    ax1pos = get(gca,'Position');
    ax2pos = [ax1pos(1) + (1-rat)*ax1pos(3),ax1pos(2) + (1-rat)*ax1pos(4),ax1pos(3)*rat,ax1pos(4)*rat];
    ax2 = axes('Position',[ax2pos],'xcolor',get(gcf,'color'),'xtick',[],'ycolor',get(gcf,'color'),'ytick',[],'Color',get(gcf,'color'));
    i = floor(size(odf,2)/2)+1;
    odf_points = (odf_vertices.*repmat(odf(:,i)-min(odf(:,i)),[n 3])');
    try
        polygon.vertices = odf_points' + repmat(odf_loc(i,:),[size(odf_points,2),1]);
        polygon.faces = odf_faces'+1;
        polygon.facevertexcdata = abs(odf_vertices)';
        p = patch(polygon);
        set(p,'FaceColor','flat','EdgeColor','none');
    end;
    daspect([1 1 1])
    axis equal;
    set(ax2,'xticklabel',[]);
    set(ax2,'yticklabel',[]);
    view([0,-90]);
    ax1;
end;
