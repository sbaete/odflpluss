% Steven Baete
% NYU SOM CBI
% August 2017

function tsout = prepforout(var,mask,flipdir)
    if ((nargin < 3) || (isempty(flipdir))) flipdir = []; end;
    tsout=zeros(size(mask));tsout(mask==1)=var;
    if (~isempty(flipdir))
        tsout = flipdim(tsout,flipdir);
    end;
end
