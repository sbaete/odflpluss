% Steven Baete
% NYU SOM CBI
% August 2017

function mat = readFSLdesign(file)

fid = fopen(file);
a = textscan(fid,'%s %d',1);
b = textscan(fid,'%s %d',1,'HeaderLines',1);
c = textscan(fid,'%f','HeaderLines',3);
fclose(fid);
mat = reshape(c{1},[a{2},b{2}])';
